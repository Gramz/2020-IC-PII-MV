/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplorelaciones;

/**
 *
 * @author ALLAN
 */
public class Aplicacion {
    public static void main(String[] args) {
        Motor m1 = new Motor();
        Motor m2 = new Motor();
        
        Coche c1 = new Coche(m2);
        Coche c2 = new Coche(m1);
        
        c2.enciende();
        
        Persona p = new Persona("Allan");
        Persona p1 = new Persona("Allan Chofer");
        
        p.asignaCoche(c2);
        
        c2.asignaConductor(p);
        p.emociona();
        p.emociona();
        p.emociona();
        p.tranquiliza();
        p.tranquiliza();
        p.tranquiliza();
        p.tranquiliza();
        System.out.println(p);
        
    }
}
