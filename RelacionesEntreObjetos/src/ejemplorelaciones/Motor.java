/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplorelaciones;

/**
 *
 * @author ALLAN
 */
public class Motor {

    private int rpm;
    private boolean activo;

    public void cambiarRPM(int rpm) {
        this.rpm = rpm;
    }

    public int leerRPM() {
        return rpm;
    }

    public boolean estaActivo() {
        return activo;
    }

    public void activa() {
        activo = true;
        rpm = 1000;
    }

    public void desactiva() {
        activo = false;
        rpm = 0;
    }

    @Override
    public String toString() {
        return "Motor{" + "rpm=" + rpm + ", activo=" + activo + '}';
    }
    
    

}
