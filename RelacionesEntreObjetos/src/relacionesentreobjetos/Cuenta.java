/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreobjetos;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Cuenta {

    private long numero;
    private float saldo;
    private float interesAnual;
    private Cliente titular;
    private LinkedList<Movimiento> movimientos;

    public Cuenta() {
        movimientos = new LinkedList<>();
    }

    public Cuenta(long numero, Cliente titular, float interesAnual) {
        this.numero = numero;
        this.titular = titular;
        this.interesAnual = interesAnual;
        movimientos = new LinkedList<>();
    }

    public void ingreso(int cantidad) {
        saldo += Math.abs(cantidad);
        movimientos.add(new Movimiento(LocalDate.now(), 'I', cantidad, saldo));
    }

    public void reintegro(int cantidad) {
        saldo -= Math.abs(cantidad);
        movimientos.add(new Movimiento(LocalDate.now(), 'R', cantidad, saldo));
    }

    public int leerSaldo() {
        return (int) saldo;
    }

    public boolean enRojos() {
        return saldo < 0;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public Cliente getTitular() {
        return titular;
    }

    public void setTitular(Cliente titular) {
        this.titular = titular;
    }

    public float getInteresAnual() {
        return interesAnual;
    }

    public void setInteresAnual(float interesAnual) {
        this.interesAnual = interesAnual;
    }

    @Override
    public String toString() {
        return "Cuenta{" + "numero=" + numero + ", titular=" + titular + ", saldo=" + leerSaldo() + ", interesAnual=" + interesAnual + '}'
                + "Movimientos: " + Arrays.toString(movimientos.toArray());
    }

}
