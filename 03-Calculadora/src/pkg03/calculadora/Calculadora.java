/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg03.calculadora;

/**
 *
 * @author ALLAN
 */
public class Calculadora {

    private double memoria;
    private char operador;
    private double numero;

    public Calculadora() {
        memoria = 0.0;
        operador = ' ';
        numero = 0.0;
    }

    public void setMemoria(double memoria) {
        this.memoria = memoria;
    }

    public double getMemoria() {
        return memoria;
    }

    public void mc() {
        memoria = 0.0;
    }

    public void mMas(double numero) {
        memoria += numero;
    }

    public void mMenos(double numero) {
        memoria -= numero;
    }

    public double masMenos(double d) {
        return d * -1;
    }

    public double raiz(double d) {
        return Math.sqrt(d);
    }

    public double inverso(double d) {
        return 1 / d;//Math.round((1 / d * 1000000000))/1000000000.0;
    }

    public char getOperador() {
        return operador;
    }

    public void setOperador(char operador) {
        this.operador = operador;
    }

    public void setNumero(double numero) {
        this.numero = numero;
    }

    public double resultado(double d) {
        switch (operador) {
            case '+':
                operador = ' ';
                return numero + d;
            case '-':
                operador = ' ';
                return numero - d;
            case '*':
                operador = ' ';
                return numero * d;
            case '/':
                operador = ' ';
                return numero / d;
            default:
                return 0;
        }
    }

}
